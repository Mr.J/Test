import java.io.UnsupportedEncodingException;


public class TEST {
	public static void main(String[] args) throws UnsupportedEncodingException{
		String str = "����˭";
		byte[] a_java = str.getBytes();
		byte[] a_UTF_8 = str.getBytes("UTF-8");
		byte[] a_ISO_8859_1 = str.getBytes("ISO-8859-1");
		byte[] a_GBK = str.getBytes("GBK");
		byte[] a_Unicode = str.getBytes("unicode");
		printByte("java", a_java);
		printByte("UTF-8", a_UTF_8);
		printByte("ISO-8859-1", a_ISO_8859_1);
		printByte("GBK", a_GBK);
		printByte("Unicode", a_Unicode);
		//
		String str2 = new String(str.getBytes(),"UTF-8");
		System.out.println("Str2 : " + str2);
		printByte("STR2-BYTE", str2.getBytes());
		//
		String str3 = new String(str.getBytes("UTF-8"),"UTF-8");
		System.out.println("Str3 : " + str3);
		printByte("STR3-BYTE", str3.getBytes());
	}
	private static void printByte(String name, byte[] bs){
		System.out.print(name + " : ");
		for(byte b : bs){
			System.out.print(b + ",");
		}
		System.out.println();
	}
}
